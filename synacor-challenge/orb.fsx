type Dir = N | E | S | W
type Step = int * Dir
type NumberRoom = { Number: int; mutable Connections: (Dir * OperatorRoom) list }
and OperatorRoom = { Operator: int -> int -> int; mutable Connections: (Dir * NumberRoom) list }

let reverse = function | N -> S | E -> W | S -> N | W -> E
let connectRO (r: NumberRoom) (d: Dir) (o: OperatorRoom) =
  r.Connections <- (d, o) :: r.Connections
let connectOR (o: OperatorRoom) (d: Dir) (r: NumberRoom) =
  o.Connections <- (d, r) :: o.Connections
let connect2W (r: NumberRoom) (d: Dir) (o: OperatorRoom) =
  connectRO r d o
  connectOR o (reverse d) r

let orb = { Number = 22; Connections = [] }
let n4a = { Number = 4; Connections = [] }
let n4b = { Number = 4; Connections = [] }
let n8 = { Number = 8; Connections = [] }
let n9 = { Number = 9; Connections = [] }
let n11 = { Number = 11; Connections = [] }
let n18 = { Number = 18; Connections = [] }
let vault = { Number = 1; Connections = [] }

let plus = { Operator = (+); Connections = [] }
let starA = { Operator = (*); Connections = [] }
let starB = { Operator = (*); Connections = [] }
let starC = { Operator = (*); Connections = [] }
let starD = { Operator = (*); Connections = [] }
let minusA = { Operator = (-); Connections = [] }
let minusB = { Operator = (-); Connections = [] }
let minusC = { Operator = (-); Connections = [] }

connectRO orb N plus
connectRO orb E minusC

connect2W n4a N starA
connect2W n4a E starB
connect2W n4a S plus

connect2W n4b N starB
connect2W n4b E minusB
connect2W n4b S minusC
connect2W n4b W plus

connect2W n8 E minusA
connect2W n8 S starB
connect2W n8 W starA

connect2W n9 N minusB
connect2W n9 E starD
connect2W n9 W minusC

connect2W n11 N minusA
connect2W n11 E starC
connect2W n11 S minusB
connect2W n11 W starB

connect2W n18 N starC
connect2W n18 S starD
connect2W n18 W minusB

connectOR minusA E vault
connectOR starC N vault

let pathLenLimit = 20
let valueLimit = 200

let rec pathFromNumber (path: Step list) (room: NumberRoom) (v: int): (Step list) option =
  if path.Length >= pathLenLimit then None
  elif v <= 0 || v >= valueLimit then None
  elif v = 30 && obj.ReferenceEquals (vault, room) then Some path
  else
    let conns = room.Connections |> List.choose (fun (d, o) -> pathFromOperator ((v, d) :: path) o v)
    if (conns |> List.isEmpty) then None else Some (conns |> List.minBy List.length)

and pathFromOperator (path: Step list) (room: OperatorRoom) (v: int): (Step list) option =
  if path.Length >= pathLenLimit then None
  else
    let conns = room.Connections |> List.choose (fun (d, n) -> pathFromNumber ((room.Operator v n.Number, d) :: path) n (room.Operator v n.Number))
    if (conns |> List.isEmpty) then None else Some (conns |> List.minBy List.length)

printfn "%A" (pathFromNumber [] orb 22 |> Option.get |> List.rev)

// Result:
//[(22, N); (26, E); (26, E); (15, N); (15, W); (60, S); (60, E); (42, E); (42, W);
// (31, N); (31, N); (30, E)]
