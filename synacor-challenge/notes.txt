Text adventure:

Starting area has a tablet.
Must go into the cave, and then north several times.
Rope bridge breaks (seems to be required).
Moss cavern beneath, goes 2 ways.
East in the cavern has an empty lantern, then you have to turn back around.
3 way split: cavern/ladder/darkness
- cavern is where you came from
- ladder leads to a maze
- darkness is bad. find oil first?

Maze:


          3
          |
         1X->1 (wrap)
          |
(wrap) 2<-X2
       |  |    |
       X--S----X--
       |  |    |
          3
         (wrap)

... weird stuff going on, not a grid
east->east has a grue
west->south also has a grue
west->south->north had a code! also the can of oil
seems like that's probably the only thing down there

--

Eventually you come out of the dark in an E/W passage
Eventually you see "_ + _ * _^2 + _^3 - _ = 399" and run into a series of rooms
- N is locked
- E. coin. down staircase.
  - another coin
- W. coin. up staircase.
  - another coin.
- I now have 5 coins for the 5 _

- red coin: 2 dots
- corroded coin: triangle
- shiny coin: pentagon
- concave coin: 7 dots
- blue coin: 9 dots

Result: [9; 2; 5; 7; 3] -> [blue, red, shiny, concave, corroded]

After solving the puzzle you get to a teleporter.
New code! strange book, really long..... crazy stuff about teleporters

The cover of this book subtly swirls with colors.  It is titled "A Brief Introduction to Interdimensional Physics".  It reads:

Recent advances in interdimensional physics have produced fascinating
predictions about the fundamentals of our universe!  For example,
interdimensional physics seems to predict that the universe is, at its root, a
purely mathematical construct, and that all events are caused by the
interactions between eight pockets of energy called "registers".
Furthermore, it seems that while the lower registers primarily control mundane
things like sound and light, the highest register (the so-called "eighth
register") is used to control interdimensional events such as teleportation.

A hypothetical such teleportation device would need to have have exactly two
destinations.  One destination would be used when the eighth register is at its
minimum energy level - this would be the default operation assuming the user
has no way to control the eighth register.  In this situation, the teleporter
should send the user to a preconfigured safe location as a default.

The second destination, however, is predicted to require a very specific
energy level in the eighth register.  The teleporter must take great care to
confirm that this energy level is exactly correct before teleporting its user!
If it is even slightly off, the user would (probably) arrive at the correct
location, but would briefly experience anomalies in the fabric of reality
itself - this is, of course, not recommended.  Any teleporter would need to test
the energy level in the eighth register and abort teleportation if it is not
exactly correct.

This required precision implies that the confirmation mechanism would be very
computationally expensive.  While this would likely not be an issue for large-
scale teleporters, a hypothetical hand-held teleporter would take billions of
years to compute the result and confirm that the eighth register is correct.

If you find yourself trapped in an alternate dimension with nothing but a
hand-held teleporter, you will need to extract the confirmation algorithm,
reimplement it on more powerful hardware, and optimize it.  This should, at the
very least, allow you to determine the value of the eighth register which would
have been accepted by the teleporter's confirmation mechanism.

Then, set the eighth register to this value, activate the teleporter, and
bypass the confirmation mechanism.  If the eighth register is set correctly, no
anomalies should be experienced, but beware - if it is set incorrectly, the
now-bypassed confirmation mechanism will not protect you!

Of course, since teleportation is impossible, this is all totally ridiculous.

---

Step 1: make a way to dump memory and then dump memory after the game starts.

Found a check at offset 0x411:
0700 0780 4504 -> 7 0x8007 0x0445 -> if reg 7 is nonzero, jump to addr 0x445.

Also offset 0x2A97:
0800 0780 E515 -> 8 0x8007 0x15E5 -> if reg 8 is zero, jump to addr 0x15E5.

Also offset 0x2B26:
0100 0080 0780 -> 1 0x8000 0x8007 -> set reg 0 to the value of reg 7.
(probably related to above check)

---

Now that I have a disasm:
Instruction 154B "if 8007 == 0 to 15E5"
So, presumably, that's the normal path. The teleporter path must be at 154E.
First, we CALL to 05B2 (after configuring a couple registers).
  - 05B2 is hard to decode. Can I tell by other callers? There is a really early call..
  - It's the first spot after the diag stuff. Maybe start of the text adventure? can't just jump there though

---

154E executes the teleportation (with several errors)
- it does this even if you don't have the teleporter
1566 goes straight to "Miscalibration detected!  Aborting teleportation!
- also 15CB (which is the direct print I think)
179A seems to be where we actually use 8007

Algo?

156B: SET  reg 8000 := 0004
156E: SET  reg 8001 := 0001
1571: CALL to 178B
1573: EQ   8001 := 8000 = 0006
1577: JF   if 8001 == 0 to 15CB
157A: PUSH from 8000
..

178B: JT   if 8000 != 0 to 1793
178E: ADD  8000 := 8001 + 0001
1792: RET
1793: JT   if 8001 != 0 to 17A0
1796: ADD  8000 := 8000 + 7FFF
179A: SET  reg 8001 := 8007
179D: CALL to 178B
179F: RET
17A0: PUSH from 8000
17A2: ADD  8001 := 8001 + 7FFF
17A6: CALL to 178B
17A8: SET  reg 8001 := 8000
17AB: POP  to 8000
17AD: ADD  8000 := 8000 + 7FFF
17B1: CALL to 178B
17B3: RET

We want 8001 to be nonzero.

Tracing (with magic = 7):

8000 <- 4
8001 <- 1
CALL 178B
if (8000 != 0) jump 1793
if (8001 != 0) jump 17A0
PUSH 4
8001 <- 0 (8001 + 7FFF)
CALL 178B
if (8000 != 0) jump 1793
if (8001 != 0) jump 17A0 - FAIL





Psudocode-ified:

func(a, b): {
  if (a == 0) {
    a = b + 1
    return a
  }
  if (b == 0) {
    a = a + 0x7FFF
    b = MAGIC
    return func (a, b)
  }

  save a
  b = b + 0x7FFF
  a = func(a, b)
  b = a
  restore a
  a = a + 0x7FFF
  return a
}

a = func(4, 1)
b = (a == 6)
/* here we want b to be true */

.t trace.txt
.w 8000 4
.w 8001 1
.j 178b


=====

178B: JT   if 8000 != 0 to 1793
178E: ADD  8000 := 8001 + 0001
1792: RET
1793: JT   if 8001 != 0 to 17A0
1796: ADD  8000 := 8000 + 7FFF
179A: SET  reg 8001 := 8007
179D: CALL to 178B
179F: RET
17A0: PUSH from 8000
17A2: ADD  8001 := 8001 + 7FFF
17A6: CALL to 178B
17A8: SET  reg 8001 := 8000
17AB: POP  to 8000
17AD: ADD  8000 := 8000 + 7FFF
17B1: CALL to 178B
17B3: RET

===== JUMP to 157A on 0 =====

You wake up on a sandy beach with a slight headache.  The last thing you remember is activating that teleporter... but now you can't find it anywhere in your pack.  Someone seems to have drawn a message in the sand here:

    nlqQibBngyEl

It begins to rain.  The message washes away.  You take a deep breath and feel firmly grounded in reality as the effects of the teleportation wear off.

System.Exception: Bad opcode: 101
   at FSI_0001.exec(CpuState cpu)
   at <StartupCode$FSI_0001>.$FSI_0001.main@()
Stopped due to error



> func 4 1 0;;
func 4 1
func 4 0
func 3 0
func 2 0
func 1 0
func 0 0
func 3 1
func 3 0
func 2 0
func 1 0
func 0 0
func 2 1
func 2 0
func 1 0
func 0 0
func 1 1
func 1 0
func 0 0
func 0 1
val it : int = 2

25734 -> 6
FOUND: 0x6486


.l end.bin
.w 8007 6486
.w 156b 6
.w 156c 157a
use teleporter

TEXT ADVENTURE PART 2

Start: beach, exits w/e/n
west -> embankment
also east
north -> chirping, west blocked
north many times, eventually alcove to east

entering gives you the #, exiting gives you the oper (-/+/*)
need to do it in the minimum number of steps

orb starts at 22
vault ends at 30


ORB = 22
  + 4 (A) = 26
  + 22 = 48
  + 4 (B) = 52
  - 11 = 41
  * 1 = 41
  - 11 = 30
  * 1 = 30

PATH =
  North North
  South South
  East North
  East North
  East North
  West South
  East North

ORB = 22
  + 22 = 44
  + 22 = 66
  - 9 = 57
  - 9 = 48
  - 18 = 30
  * 1 = 30

PATH =
  North South
  North South
  East East
  West East
  North East
  North North

ORB = 22
  + 4 (B) = 26
  - 11 = 15
  - 11 = 4
  * 8 = 32
  - 1 = 31
  - 1 = 30

PATH =
  North East
  East North
  North South
  West North
  East East
  West East

ORB = 22
  + 4 (A) = 26
  + 4 (A) = 30
  + 4 (A) = 34
  + 4 (A) = 38
  + 4 (B) = 42
  - 11 = 31
  - 1 = 30

PATH =
  North North
  South North
  South North
  South North
  South East
  East North
  North East

ORB -> 4B -> 4A -> 4B -> 4A -> 4B -> (-) 11 -> (-) 1
PATH
  North East
  West North
  South East
  West North
  South East
  East North
  North East

Memory location for orb: 0F70
Right before walk in from the south: ".w f70 1e"
uUdouillAdYW
WYbAlliuobUu
WYdAlliuodUu
WYdAlliuodUu

gpctrseodceu

While printing the mirror message:
PC=5F8 stack=["05DC"; "0057"; "0001"; "000C"; "0001"; "17F1"; "05F5"; "653F"; "07C6"; "5F61";
 "0000"; "0000"; "0065"; "74F6"; "1693"; "0006"; "0006"; "0003"; "1659"; "0AA0";
 "0E0F"; "0DF0"; "657A"; "0B88"; "0001"; "17EC"; "0010"; "17C0"]

Jumps straight to the code!
.j 1659
You gaze into the mirror, and you see yourself gazing back.  But wait!  It looks like someone wrote on your face while you were unconscious on the beach!  Through the mirror, you see "WTwTYWoUwdl8" scrawled in charcoal on your forehead.

WTwTYWoUwdl8
8lbwUoWYTwTW

1679 jumps stright to code gen
The registers don't control this - must be on the stack or a memory location
memory[0F73..0F75] control it

1236: WMEM 0F70 := 0016 // THE ORB
1239: WMEM 0F71 := 0000 // number of steps taken
123C: WMEM 0F72 := 0000 // part 0 of the final password??
123F: WMEM 0F73 := 0000 // part 1 of the final password
1242: WMEM 0F74 := 0000 // part 2 of the final password
1245: WMEM 0F75 := 0000 // part 3 of the final password

All 3 of the final password variables change on every step of the vault maze
None of them are set before we pick up the orb
None increment while not holding the orb

MbwTYWApwWld
blWwqAWYTwdM

== With new (better) solution:

OTMbbbiIwWx8

8xWwIidddMTO
