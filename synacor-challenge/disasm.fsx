open System.IO

let disasmNextInstruction (words: int list) (pc: int): int * string =
  let arg offset = sprintf "%04X" words.[pc + offset]
  let charArg offset =
    match words.[pc + offset] with
    | 10 -> "'\\n'"
    | x when x < 0x100 -> sprintf "'%c'" (char x)
    | x -> sprintf "%04X" x
  let printChar ch = if (ch >= 32 && ch <= 126) then char ch else '.'
  let len, desc =
    match words.[pc] with
    |  0 -> 1, sprintf "HALT"
    |  1 -> 3, sprintf "SET  reg %s := %s" (arg 1) (arg 2)
    |  2 -> 2, sprintf "PUSH from %s" (arg 1)
    |  3 -> 2, sprintf "POP  to %s" (arg 1)
    |  4 -> 4, sprintf "EQ   %s := %s = %s" (arg 1) (arg 2) (arg 3)
    |  5 -> 4, sprintf "GT   %s := %s > %s" (arg 1) (arg 2) (arg 3)
    |  6 -> 2, sprintf "JMP  to %s" (arg 1)
    |  7 -> 3, sprintf "JT   if %s != 0 to %s" (arg 1) (arg 2)
    |  8 -> 3, sprintf "JF   if %s == 0 to %s" (arg 1) (arg 2)
    |  9 -> 4, sprintf "ADD  %s := %s + %s" (arg 1) (arg 2) (arg 3)
    | 10 -> 4, sprintf "MULT %s := %s * %s" (arg 1) (arg 2) (arg 3)
    | 11 -> 4, sprintf "MOD  %s := %s %% %s" (arg 1) (arg 2) (arg 3)
    | 12 -> 4, sprintf "AND  %s := %s & %s" (arg 1) (arg 2) (arg 3)
    | 13 -> 4, sprintf "OR   %s := %s | %s" (arg 1) (arg 2) (arg 3)
    | 14 -> 3, sprintf "NOT  %s := ~%s" (arg 1) (arg 2)
    | 15 -> 3, sprintf "RMEM %s := *%s" (arg 1) (arg 2)
    | 16 -> 3, sprintf "WMEM %s := %s" (arg 1) (arg 2)
    | 17 -> 2, sprintf "CALL to %s" (arg 1)
    | 18 -> 1, sprintf "RET"
    | 19 -> 2, sprintf "OUT  %s" (charArg 1)
    | 20 -> 2, sprintf "IN   %s" (arg 1)
    | 21 -> 1, sprintf "NOOP"
    | x  -> 1, sprintf "[%04X] %c%c" x (printChar (x <<< 8)) (printChar (x &&& 0xFF))
  printfn "%04X: %s" pc desc
  len, sprintf "%04X: %s" pc desc

let disasmProgram (filename: string): string list =
  use rd = new BinaryReader(File.OpenRead filename)
  let words =
    seq { while rd.BaseStream.Position <> rd.BaseStream.Length do yield rd.ReadUInt16() }
    |> Seq.map int
    |> Seq.toList
  Seq.toList <| seq {
    let mutable pc = 0
    while pc < words.Length do
      let len, str = disasmNextInstruction words pc
      pc <- pc + len
      yield str }

System.Environment.GetCommandLineArgs()
  |> Array.find (fun x -> x.EndsWith ".bin")
  |> disasmProgram
  |> Seq.map (printfn "%s")
