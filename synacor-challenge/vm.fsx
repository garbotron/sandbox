open System.IO

type CpuState =
  { Memory: Map<int, int>
    Stack: int list
    PC: int }

  member this.Resolve = function
    | x when x < 0x8000 -> x
    | x when x < 0x8008 -> this.Memory |> Map.tryFind x |> Option.defaultValue 0
    | x -> raise <| System.Exception(sprintf "Bad number: %d" x)

  member this.Read addr = this.Memory |> Map.tryFind addr |> Option.defaultValue 0
  member this.RawArg num = this.Read <| this.PC + num
  member this.Arg num = this.RawArg num |> this.Resolve
  member this.Write addr v = { this with Memory = this.Memory |> Map.add addr (this.Resolve v) }
  member this.Push v = { this with Stack = v :: this.Stack }
  member this.Pop = List.head this.Stack, { this with Stack = List.tail this.Stack }

let mutable inputBuffer: char list = []
let mutable instructionTracer: TextWriter = null

let loadProgram (filename: string) (pc: int): CpuState =
  use reader = new BinaryReader(File.OpenRead filename)
  let mem = seq {
    while reader.BaseStream.Position <> reader.BaseStream.Length do
      yield reader.ReadUInt16() |> int } |> Seq.indexed |> Map
  { Memory = mem; Stack = []; PC = pc }

let saveState (cpu: CpuState) (filename: string): unit =
  use writer = new BinaryWriter(File.OpenWrite filename)
  let writeAddr addr = cpu.Memory |> Map.tryFind addr |> Option.defaultValue 0 |> uint16 |> writer.Write
  { 0 .. 0x800F } |> Seq.iter writeAddr
  writer.Write (uint16 cpu.PC)
  writer.Write (uint16 cpu.Stack.Length)
  cpu.Stack |> List.iter (uint16 >> writer.Write)

let loadState (filename: string): CpuState =
  use reader = new BinaryReader(File.OpenRead filename)
  let mem = { 0 .. 0x800F } |> Seq.map (fun _ -> int <| reader.ReadUInt16()) |> Seq.indexed |> Map
  let pc = int <| reader.ReadUInt16()
  let stackSize = int <| reader.ReadUInt16()
  let stack = [ 1 .. stackSize ] |> List.map (fun _ -> int <| reader.ReadUInt16())
  { Memory = mem; Stack = stack; PC = pc }

let hexint s = System.Int32.Parse (s, System.Globalization.NumberStyles.HexNumber)

let rec readInputLine (cpu: CpuState): Choice<string, CpuState>  =
  let line = System.Console.ReadLine()
  if line.StartsWith "." then // local command
    let cpu =
      match line.[1..].Split ' ' with
      | [| "s"; file |] -> saveState cpu file; cpu
      | [| "l"; file |] -> loadState file
      | [| "s" |] -> saveState cpu "snap.bin"; cpu
      | [| "l" |] -> loadState "snap.bin"
      | [| "j"; dest |] ->
        printfn "Jumping to %04X (%04X)" (hexint dest) (cpu.Read (hexint dest))
        { cpu with PC = hexint dest }
      | [| "i" |] ->
        printfn "PC: %04X" cpu.PC
        for i in { 0x8000 .. 0x8007 } do
          printfn "Reg[%04X]: %04X" i (cpu.Memory |> Map.tryFind i |> Option.defaultValue 0)
        cpu
      | [| "r"; addr |] -> printfn "%04X: %04X" (hexint addr) (cpu.Read (hexint addr)); cpu
      | [| "w"; addr; v |] -> cpu.Write (hexint addr) (hexint v)
      | [| "p"; v |] -> cpu.Push (hexint v)
      | [| "t"; file |] ->
        if isNull instructionTracer then
          instructionTracer <- new StreamWriter(File.OpenWrite file)
          printfn "Instruction tracing is ON"
        else
          instructionTracer.Dispose()
          instructionTracer <- null
          printfn "Instruction tracing is OFF"
        cpu
      | _ -> printfn "*** Unknown command"; cpu
    Choice2Of2 cpu
  else Choice1Of2 line

let rec getc (cpu: CpuState): Choice<char, CpuState> =
  match inputBuffer with
  | [] -> match readInputLine cpu with
          | Choice1Of2 line -> inputBuffer <- line + "\n" |> Seq.toList; getc cpu
          | Choice2Of2 cpu -> Choice2Of2 cpu
  | h :: t -> inputBuffer <- t; Choice1Of2 h

let add a b = (a + b) % 0x8000
let mul a b = (a * b) % 0x8000
let eq a b = if a = b then 1 else 0
let gt a b = if a > b then 1 else 0
let bitnot a = ~~~a &&& 0x7FFF
let putc v = printf "%c" (char v)

let mutable last8Outputs: char list = []

let rec exec (cpu: CpuState): CpuState =
  if not (isNull instructionTracer) then
    let words = [ 0 .. 3 ] |> List.map (fun x -> cpu.Read (cpu.PC + x))
    instructionTracer.WriteLine (sprintf "INST %d (%04X, %04X, %04X)" words.[0] words.[1] words.[2] words.[3])
  match cpu.Read cpu.PC with
  | 0 -> cpu // halt
  | 1 -> exec { cpu.Write (cpu.RawArg 1) (cpu.Arg 2) with PC = cpu.PC + 3 }
  | 2 -> exec { cpu.Push (cpu.Arg 1) with PC = cpu.PC + 2 }
  | 3 -> cpu.Pop |> fun (top, cpu) -> exec { cpu.Write (cpu.RawArg 1) top with PC = cpu.PC + 2 }
  | 4 -> exec { cpu.Write (cpu.RawArg 1) (eq (cpu.Arg 2) (cpu.Arg 3)) with PC = cpu.PC + 4 }
  | 5 -> exec { cpu.Write (cpu.RawArg 1) (gt (cpu.Arg 2) (cpu.Arg 3)) with PC = cpu.PC + 4 }
  | 6 -> exec { cpu with PC = cpu.Arg 1 }
  | 7 -> exec { cpu with PC = if cpu.Arg 1 <> 0 then cpu.Arg 2 else cpu.PC + 3 }
  | 8 -> exec { cpu with PC = if cpu.Arg 1 = 0 then cpu.Arg 2 else cpu.PC + 3 }
  | 9 -> exec { cpu.Write (cpu.RawArg 1) (add (cpu.Arg 2) (cpu.Arg 3)) with PC = cpu.PC + 4 }
  | 10 -> exec { cpu.Write (cpu.RawArg 1) (mul (cpu.Arg 2) (cpu.Arg 3)) with PC = cpu.PC + 4 }
  | 11 -> exec { cpu.Write (cpu.RawArg 1) (cpu.Arg 2 % cpu.Arg 3) with PC = cpu.PC + 4 }
  | 12 -> exec { cpu.Write (cpu.RawArg 1) (cpu.Arg 2 &&& cpu.Arg 3) with PC = cpu.PC + 4 }
  | 13 -> exec { cpu.Write (cpu.RawArg 1) (cpu.Arg 2 ||| cpu.Arg 3) with PC = cpu.PC + 4 }
  | 14 -> exec { cpu.Write (cpu.RawArg 1) (cpu.Arg 2 |> bitnot) with PC = cpu.PC + 3 }
  | 15 -> exec { cpu.Write (cpu.RawArg 1) (cpu.Read (cpu.Arg 2)) with PC = cpu.PC + 3 }
  | 16 -> exec { cpu.Write (cpu.Arg 1) (cpu.Arg 2) with PC = cpu.PC + 3 }
  | 17 -> exec { cpu.Push (cpu.PC + 2) with PC = cpu.Arg 1 }
  | 18 -> cpu.Pop |> fun (top, cpu) -> exec { cpu with PC = top }
  | 19 -> last8Outputs <- last8Outputs @ [cpu.Arg 1 |> char]
          if last8Outputs.Length > 8 then last8Outputs <- last8Outputs.[1..]
          if last8Outputs = ['u';'U';'d';'o';'u';'i';'l';'l'] then
            printfn "\n\nPC=%X stack=%A\n\n" cpu.PC (cpu.Stack |> List.map (sprintf "%04X"))
          putc (cpu.Arg 1); exec { cpu with PC = cpu.PC + 2 }
  | 20 -> match getc cpu with
          | Choice1Of2 ch -> exec { cpu.Write (cpu.RawArg 1) (int ch) with PC = cpu.PC + 2 }
          | Choice2Of2 cpu -> exec cpu
  | 21 -> exec { cpu with PC = cpu.PC + 1 }
  | x -> raise <| System.Exception(sprintf "Bad opcode: %d" x)

let findArg name def conv =
  let prefix = "--" + name + "="
  System.Environment.GetCommandLineArgs()
  |> Array.tryFind (fun x -> x.StartsWith prefix)
  |> Option.defaultValue (prefix + def)
  |> fun x -> x.[prefix.Length..]
  |> conv

let file = findArg "file" "challenge.bin" id
let pc = findArg "pc" "0" int
exec <| loadProgram file pc

// let magic = 1
// loadProgram "/mnt/c/temp/temp.bin" 0
//   |> fun x -> x.Write 0x8000 4
//   |> fun x -> x.Write 0x8001 1
//   |> fun x -> x.Write 0x8007 magic
//   |> fun x -> x.Push 0x29
//   |> exec
//   |> fun x -> x.Read 0x8000
//   |> printfn "%d"
