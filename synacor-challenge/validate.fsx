let add a b = (a + b) % 0x8000

let mutable magic = 0
let cache = new System.Collections.Generic.Dictionary<int * int, int>()

let rec func a b =
  if cache.ContainsKey (a, b) then
    cache.[a, b]
  else
    let ret =
      if a = 0 then
        add b 1
      elif b = 0 then
        func (add a 0x7FFF) magic
      else
        func (add a 0x7FFF) (func a (add b 0x7FFF))
    cache.Add((a, b), ret)
    ret

let call x =
  cache.Clear()
  magic <- x
  func 4 1

{ 0 .. 0x7FFF }
  |> Seq.map (fun x ->
                  let ret = call x
                  printfn "%d -> %d" x ret
                  x, ret)
  |> Seq.filter (snd >> ((=) 6))
  |> Seq.head
  |> fst
  |> printf "FOUND: 0x%X"
