let rec distribute e = function
  | [] -> [[e]]
  | x :: xs' as xs -> (e :: xs) :: [for xs in distribute e xs' -> x :: xs]

let rec permute = function
  | [] -> [[]]
  | e :: xs -> List.collect (distribute e) (permute xs)

// PUZZLE: _ + _ * _^2 + _^3 - _ = 399
// COINS: red=2 corroded=3 shiny=5 concave=7 blue=9

let check (x: int list) = x.[0] + x.[1] + (x.[2] * x.[2]) + (x.[3] * x.[3] * x.[3]) - x.[4] = 399
printfn "%A" (permute [2; 3; 5; 7; 9] |> List.find check)

// Result: [9; 2; 5; 7; 3] -> [blue, red, shiny, concave, corroded]
